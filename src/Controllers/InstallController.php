<?php

namespace Spn4\SchoolService\Controllers;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Spn4\SchoolService\Repositories\InstallRepository;
use Spn4\Service\Repositories\InstallRepository as ServiceRepository;
use Spn4\SchoolService\Requests\UserRequest;
use Illuminate\Support\Facades\Storage;

class InstallController extends Controller{
    protected $repo, $request, $service_repo;

    public function __construct(
        InstallRepository $repo,
        Request $request,
        ServiceRepository $service_repo
    )
    {
        $this->repo = $repo;
        $this->request = $request;
        $this->service_repo = $service_repo;
    }

    public function index(){
        return view('school::install.welcome');
    }


    public function user(){
        if(!$this->service_repo->checkDatabaseConnection()){
            abort(404);
        }

		return view('school::install.user');
    }

    public function post_user(UserRequest $request){
       try{
            $this->service_repo->install($request->all());
            Storage::put('.app_installed', 'white label');
       } catch(\Exception $e){
           dd($e);
            return response()->json(['message' =>$e->getMessage()]);
       }

        $this->repo->install($request->all());
		return response()->json(['message' => __('school::install.done_msg'), 'goto' => route('service.done')]);
    }


}
